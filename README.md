# wine-pba

A set of patches to allocate dynamic wined3d_buffers from a single persistently mapped buffer (via `ARB_buffer_storage`) managed by a heap allocator, reducing the need for command stream synchronization.

Several related changes are included in the patchset as well:

- `ARB_multi_bind` is used to speed up UBO updates
    - This vastly improves constant buffer performance as PBA causes much more frequent rebinds.

**This patchset is prototype-quality at the moment. If `ARB_buffer_storage` is not present, you're not going to have a good time.**

[Details can be found here.](https://comminos.com/posts/2018-02-21-wined3d-profiling.html)

Firerat additions

Currently, these patches are based off wine-staging.
 Although not officially supported, you may have luck applying the patchset to wine-devel

Please refer to the "Release Tags" for specific versions.

I have a [bash script which updates, via a cronjob,](https://gist.github.com/Firerat/bb4a2e795dbb8aabe9a90fa92d97b635) [my wine-staging fork](https://github.com/Firerat/wine-staging)
My Wine-Staging fork also includes modified CSMT toggle in winecfg ( which just makes more sense )

If you feel you need to use a daily snapshot, use that wine-staging fork or better still ... Adapt my update script.


-  `__PBA_GEO_HEAP` and `__PBA_CB_HEAP` envvars available
    - allows you to tweak the heap sizes without recompiling, which resolves problems with at least one game _FF XIV in d3d9 mode_
    [upstream issue 36 fix suggestion ](https://github.com/acomminos/wine-pba/issues/36#issuecomment-372079218)
    [upstream issue 36 fix confirmation ](https://github.com/acomminos/wine-pba/issues/36#issuecomment-372148581)
    since d3d9 does not use Constant Buffers `__PBA_CB_HEAP=0` would make sense ( NOTE: some games mix d3d versions )
    -   apparently pumping up the heap sizes for Witcher 3 reveals the vegetation ( but performance drops, more rendering I guess )
    -   `__PBA_FORCE_GL_CLIENT_STORAGE_BIT` added [as per request in this issue](https://github.com/Firerat/wine-pba/issues/2) only effects Nvidia ( well.. None mesa ), and will probably make performance worse!
  
